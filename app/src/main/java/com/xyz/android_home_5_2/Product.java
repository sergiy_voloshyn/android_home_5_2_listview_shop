package com.xyz.android_home_5_2;

/**
 * Created by user on 19.01.2018.
 */

public class Product {
    String name;
    Integer count;

    public Product(String name, Integer count) {
        this.name = name;
        this.count = count;
    }

    public Product() {
        this.name = "";
        this.count = 0;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public Integer getCount() {
        return count;
    }
}
