package com.xyz.android_home_5_2;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
/*
2. Создать программу "Товарооборот". На экране есть список товаров (5 наименований, больше не берите)
 и их кол-во. Каждое n количество времени в магазине покупают несколько товаров. Список каждый раз
 должен обновляться.
//in activity
listView.notifyDataSetChanged();// метод, который перегружает адаптер, если список данных изменился
2.1* Реализовать остановку работы магазина и возобновление. Покупатели, пришедшие во время перерыва
- игнорируются.
2.2** Реализовать остановку работы магазина и возобновление с созданием очереди покупателей
(за время перерыва покупатели создают очередь запросов, а при возобновлении работы все запросы
 покупателей, возникшие за время перерыва, должны быть удовлетворены)


 */

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.list_view)
    ListView listView;

    ProductAdapter adapter;
    List<Product> productList = Generator.generate();

    public static final int START = 1;
    public static final int STOP = 2;
    int time = 1000;
    boolean isStarted;

    Handler h = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            if (msg.what == START) {
                saleProduct();
                adapter.notifyDataSetChanged();
                h.sendEmptyMessageDelayed(START, time);
            }
            if (msg.what == STOP) {
                stopTimer();
            }
        }
    };

    public void saleProduct() {

        final Random random = new Random();
        for (int i = 0; i < productList.size(); i++) {

            Product product = new Product();
            product.setName( productList.get(i).getName());
            product.setCount( productList.get(i).getCount() - random.nextInt(10));

            if (product.count < 0) product.count = 0;

            productList.set(i, product);


        }

    }

    public void startTimer() {
        isStarted = true;
        h.sendEmptyMessage(START);

    }

    public void stopTimer() {
        isStarted = false;
        h.removeMessages(START);
    }

    @OnClick(R.id.buttonBuy)
    public void onClick() {
        if (isStarted) {
            stopTimer();
        } else {
            startTimer();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        adapter = new ProductAdapter(this, productList);
        listView.setAdapter(adapter);

    }


    public class ProductAdapter extends ArrayAdapter<Product> {

        public ProductAdapter(@NonNull Context context, @NonNull List<Product> productsList) {
            super(context, R.layout.activity_main, productsList);
            listView = (ListView) findViewById(R.id.list_view);

        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            ViewHolder viewHolder;
            View root = convertView;
            if (root == null) {
                root = getLayoutInflater().inflate(R.layout.item_layout, parent, false);
                viewHolder = new ViewHolder(root);
                root.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) root.getTag();
            }
            Product product = getItem(position);
            viewHolder.name.setText(product.getName());
            viewHolder.count.setText(product.getCount().toString(), TextView.BufferType.EDITABLE);

            return root;
        }

    }


    static class ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.count)
        EditText count;

        public ViewHolder(View v) {
            ButterKnife.bind(this, v);
        }
    }


}
